angular.module('songhop.services', ['ionic.utils'])
    .factory('User', function($http, $q, $localstorage, SERVER){
        var o = {
            username: false,
            session_id: false,
            favorites: [],
            newFavorites: 0
        }

        o.addSongToFavorites = function(song){

            if(!song){
                return false;
            }
            o.favorites.unshift(song);

            return $http.post(SERVER.url + '/favorites', {session_id: o.session_id, song_id: song.song_id});
        }

        o.removeSongFromFavorites = function(song, index){
            if(!song){
                return false;
            }

            o.favorites.splice(index, 1);

            return $http({
                method: 'DELETE',
                url: SERVER.url + '/favorites',
                params: {session_id: o.session_id, song_id:song.song_id}
            });
        }

        o.auth = function(username, signUp){
            
            var authRoute;

            if(signUp){
                authRoute = 'signUp'
            }
            else{
                authRoute = 'login'
            }

            return $http.post(SERVER.url + '/' + authRoute, {username: username})
                .success(function(data){
                    o.setSession(data.username, data.session_id, data.favorites);
                })

        }

        o.populateFavorites = function() {
            return $http({
                method: 'GET',
                url: SERVER.url + '/favorites',
                params: { session_id: o.session_id }
                }).success(function(data){
                    o.favorites = data;
                });
        }

        o.setSession = function(username, session_id, favorites){
            if(username){
                o.username = username;
            }
            if(session_id){
                o.session_id = session_id;
            }
            if(favorites){
                o.favorites = favorites;
            }

            $localstorage.setObject('user', {username: username, session_id: session_id});
        }

        o.checkSession = function(){
            var defer = $q.defer();

            if (o.session_id) {
                defer.resolve(true);
            } 
            else {
                var user = $localstorage.getObject('user');

                if (user.username) {
                    o.setSession(user.username, user.session_id);
                    o.populateFavorites().then(function() {
                        defer.resolve(true);
                    });
                } 
                else {
                    defer.resolve(false);
                }
            }
            return defer.promise;
        }

        o.destroySession = function(){
            $localstorage.setObject('user', {});
            o.username = false;
            o.session_id = false;
            o.favorites = [];
            o.newFavorites = 0;
        }

        return o;
    })
    .factory('Recommendations', function($http, SERVER){
        var o = {
            queue: []
        };

        o.getNextSongs = function(){
            return $http({
                method: 'GET',
                url: SERVER.url + '/recommendations',

            }).success(function(data){
                o.queue = o.queue.concat(data);
            });
        };

        o.nextSong = function(){
            o.queue.shift();

            if(o.queue.length <= 3){
                o.getNextSongs();
            }
        }

        return o;
    })
     .factory('InvestService', function($http, ITAU){

        var o = {
            investimentos: []
        };


        o.getCDBs = function(){
            return $http({
                method: 'GET',
                url: ITAU.url + '/cdbs',
                headers: {
                    'Keyid' :'99191416-4188-4760-87dc-c5b27e7c0462'
                }
            }).success(function(cdbs){
                o.investimentos = o.investimentos.concat(cdbs.data.data);
            });
        }

        o.getCoes = function(){
            return $http({
                method: 'GET',
                url: ITAU.url + '/coes',
                headers: {
                'Keyid' :'99191416-4188-4760-87dc-c5b27e7c0462'
                }
            }).then(function(coes){
                o.investimentos = o.investimentos.concat(coes.data.data.data);
            });
        };

        o.getFundos = function(){
            return $http({
                method: 'GET',
                url: ITAU.url + '/fundos',
                headers: {
                'Keyid' :'99191416-4188-4760-87dc-c5b27e7c0462'
                }
            }).then(function(fundos){
                o.investimentos = o.investimentos.concat(fundos.data.data.data);
            });
        };

        return o;
    })


    .factory('ResultsService', function($http, ITAU){
         var o = {
            results: []
        };


        o.getResults = function(){
            return $http({
                method: 'GET',
                url: ITAU.url + '/fundos?expand=rentabilidades',
                headers: {
                    'Keyid' :'99191416-4188-4760-87dc-c5b27e7c0462'
                }
            }).success(function(results){
                o.results = o.results.concat(results.data.data);
            });
        }

        return o;

    });
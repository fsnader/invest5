angular.module('songhop.controllers', ['ionic', 'songhop.services'])


  /*
  Controller for the discover page
  */
  .controller('DiscoverCtrl', function ($scope, $timeout, User, Recommendations) {


    Recommendations.getNextSongs().then(function () {
      $scope.currentSong = Recommendations.queue[0];
    })

    $scope.sendFeedback = function (bool) {

      if (bool) {
        User.addSongToFavorites($scope.currentSong);
      }

      $scope.currentSong.rated = bool;
      $scope.currentSong.hide = true;

      $timeout(function () {
        Recommendations.nextSong();
        $timeout(function () {
          $scope.currentSong = Recommendations.queue[0];
        }, 250);
      }, 250);
    }

    $scope.nextAlbumImg = function () {
      if (Recommendations.queue.length > 1) {
        return Recommendations.queue[1].image_large;
      }
      return '';
    }

  })


  /*
  Controller for the favorites page
  */
  .controller('FavoritesCtrl', function ($scope, User) {
    $scope.favorites = User.favorites;
    $scope.username = User.username;

    $scope.removeSong = function (song, index) {
      User.removeSongFromFavorites(song, index);
    }
  })

  .controller('SplashCtrl', function ($scope, $state, User) {

    $scope.submitForm = function (username, signUp) {
      User.auth(username, signUp).then(function () {
        $state.go('meusInvestimentos');
      }, function () {
        alert("Wrong username");
      });
    }

    $scope.entrarSemConta = function () {
      $state.go('questionario');
    }

  })

  .controller('QuestionarioCtrl', function ($scope, $state, $timeout) {

    $scope.input = {
      name: "",
      age: "",
      goal: "",
      deadline: "",
      initialInvestment: "",
      monthlyInvestment: "",
      risk: ""
    }

    $scope.data = {
      name: "",
      age: "",
      goal: "",
      deadline: "",
      initialInvestment: "",
      monthlyInvestment: "",
      risk: ""
    }

    $timeout(function () {
      $scope.messageState = 1;
    }, 2000);

    $scope.submitName = function (name) {
      $scope.data.name = name;
      $scope.input.name = null;
      $scope.messageState = 2;

      $timeout(function () {
        $scope.Message = "Prazer em conhece-lo, " + $scope.data.name + ". O próximo passo é saber sua idade."
        $scope.messageState = 3;
      }, 1000);
    }

    $scope.submitAge = function (age) {
      $scope.data.age = age;
      $scope.input.age = null;
      $scope.messageState = 4;

      $timeout(function () {
        $scope.Message = "E a sua renda mensal média?"
        $scope.messageState = 5;
      }, 1000);
    }

    $scope.submitSalary = function (salary) {
      $scope.data.salary = salary;
      $scope.input.salary = null;
      $scope.messageState = 6;

      $timeout(function () {
        $scope.Message = "Como você considera seu próprio comportamento com dinheiro?"
        $scope.messageState = 7;
      }, 1000);
    }
    $scope.submitMoneyBehavior = function (moneyBehavior) {
      $scope.data.moneyBehavior = moneyBehavior;
      $scope.input.moneyBehavior = null;
      $scope.messageState = 8;

      $timeout(function () {
        $scope.Message = "Quando alguem fala sobre investimentos, qual seu nível de interesse?"
        $scope.messageState = 9;
      }, 1000);
    }

    $scope.submitInterest = function (interest) {
      $scope.data.interest = interest;
      $scope.input.interest = null;
      $scope.messageState = 10;

      $timeout(function () {
        $scope.Message = "Você lê matérias sobre economia, bolsa de valores ou investimentos em geral?"
        $scope.messageState = 11;
      }, 1000);
    }

    $scope.submitReading = function (reading) {
      $scope.data.interest = reading;
      $scope.messageState = 12;

      $timeout(function () {
        $scope.Message = "Nos últimos dois anos, você aplicou em produtos, como ações, LCI/LCA ou debentures?"
        $scope.messageState = 13;
      }, 1000);
    }

    $scope.submitPreviousInvestments = function (previousInvestments) {
      $scope.data.previousInvestments = previousInvestments;
      $scope.input.previousInvestments = null;
      $scope.messageState = 14;

      $timeout(function () {
        $scope.Message = "Agora, vamos conhecer um pouco mais sobre seus objetivos de investimento."
        $timeout(function () {
          $scope.Message = "Qual o valor que você deseja juntar?"
          $scope.messageState = 15;
        }, 3000);
      }, 1000);
    }

    $scope.submitGoal = function (goal) {
      $scope.data.goal = goal;
      $scope.input.goal = null;
      $scope.messageState = 16;

      $timeout(function () {
        $scope.Message = "Em quantos meses você pretende chegar nesse valor?"
        $scope.messageState = 17;
      }, 1000);
    }

    $scope.submitDeadLine = function (deadline) {
      $scope.data.deadline = deadline;
      $scope.input.deadline = null;
      $scope.messageState = 18;

      $timeout(function () {
        $scope.Message = "Qual o valor inicial que você possui para investir?"
        $scope.messageState = 19;
      }, 1000);
    }

    $scope.submitInitialInvestment = function (investment) {
      $scope.data.initialInvestment = investment;
      $scope.input.initialInvestment = null;
      $scope.messageState = 20;

      $timeout(function () {
        $scope.Message = "Qual o valor que você planeja investir mensalmente?"
        $scope.messageState = 21;
      }, 1000);
    }

    $scope.submitMonthlyInvestment = function (investment) {
      $scope.data.monthlyInvestment = investment;
      $scope.input.monthlyInvestment = null;
      $scope.messageState = 22;

      $timeout(function () {
        $scope.Message = "Qual o nível de risco que você está disposto a ter para obter ganhos maiores?"
        $scope.messageState = 23;
      }, 1000);
    }

    $scope.submitRisk = function (investment) {
      $scope.data.risk = investment;
      $scope.input.risk = null;
      $scope.messageState = 24;

      $timeout(function () {
        $scope.Message = "Por fim, qual o seu e-mail?"
        $scope.messageState = 25;
      }, 1000);
    }

    $scope.submitEmail = function (email) {
      $scope.data.email = email;
      $scope.input.email = null;
      $scope.messageState = 26;

      $timeout(function () {
        $scope.Message = "Estamos calculando seu resultado...";
        $timeout(function () {
          $state.go('recommended')
        }, 2000);
      }, 2000);

    }

  })


  .controller('InvestimentosCtrl', function ($scope, $state, InvestService) {
    $scope.groupType = true;
    $scope.grouping = 'Risco';
    InvestService.getCDBs().then(function () {
      InvestService.getCoes().then(function () {
        InvestService.getFundos().then(function () {
          var groupByRisk = InvestService.investimentos.reduce(function (obj, item) {
            obj[item.grau_risco] = obj[item.grau_risco] || [];
            obj[item.grau_risco].push(item);
            return obj;
          }, {});
          var riskGroup = Object.keys(groupByRisk).map(function (key) {
            return { riskGroup: key, item: groupByRisk[key] };
          });
          $scope.investimentosByRisk = riskGroup;

          var groupByType = InvestService.investimentos.reduce(function (obj, item) {
            obj[item.nome_familia] = obj[item.nome_familia] || [];
            obj[item.nome_familia].push(item);
            return obj;
          }, {});
          var typeGroup = Object.keys(groupByType).map(function (key) {
            return { typeGroup: key, item: groupByType[key] };
          });
          $scope.investimentosByType = typeGroup;
        });
      })
    });

    $scope.changeGroup = function () {
      if ($scope.groupType) {
        $scope.groupType = false;
        $scope.grouping = 'Risco';
      }
      else {
        $scope.groupType = true;
        $scope.grouping = 'Tipo';
      }
    }

    $scope.toggleGroup = function (group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };

    $scope.isGroupShown = function (group) {
      return $scope.shownGroup === group;
    };

    $scope.toggleProduct = function (product) {
      if ($scope.isProductShown(product)) {
        $scope.shownProduct = null;
      } else {
        $scope.shownProduct = product;
      }
    };

    $scope.isProductShown = function (product) {
      return $scope.shownProduct === product;
    };
  })

  .controller('ResultsCtrl', function ($scope, $window, $state, ResultsService) {
    $scope.results = {};
    ResultsService.getResults().then(function (results) {
      results.data.data.data.sort(function (a, b) {
        return b.rentabilidades["12_meses"] - a.rentabilidades["12_meses"];
      });

      $scope.results = results.data.data.data.slice(2, 7);
      $scope.toggleGroup($scope.results[0]);
    });

    $scope.toggleGroup = function (group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };

    $scope.isGroupShown = function (group) {
      return $scope.shownGroup === group;
    };

    $scope.selectInvestment = function() {
      $state.go('resumo');
    }

  })


  .controller('RecommendedCtrl', function ($scope, $window, $state, ResultsService) {

    $scope.recommended = {};
    ResultsService.getResults().then(function (results) {
      results.data.data.data.sort(function (a, b) {
        return b.rentabilidades["12_meses"] - a.rentabilidades["12_meses"];
      });

      $scope.results = results.data.data.data.slice(2, 7);

      $scope.recommended = $scope.results[0];
    });
    $scope.acceptRecommendation = function () {
      $state.go('resumo');
    }
    $scope.goToRecommendations = function () {
      $state.go('results')
    }
  })

  .controller('CreateaccountCtrl', function ($scope, $state, $timeout) {
    $scope.message = 1;

    $scope.createAccount = function () {
      $scope.message = 2;
    }

    $scope.seeInvestments = function () {
      $state.go('meusInvestimentos');
    }

    $scope.restart = function () {
      $state.go('questionario');
    }

  })

  .controller('ResumoCtrl', function ($scope, $state, $timeout) {
    $scope.acceptTerms = function() {
      $state.go('createaccount');
    }

  })

  .controller('MeusRendimentosCtrl', function ($scope, $state, ResultsService) {
      $scope.user = 'John Doe';

      $scope.investiments = [
        { name: 'investiment for trip', type: 'fixed' },
        { name: 'investiment for play4', type: 'fixed' }
      ];

      $scope.results = {};
      ResultsService.getResults().then(function (results) {
          results.data.data.data.sort(function (a, b) {
              return b.rentabilidades["12_meses"] - a.rentabilidades["12_meses"];
          });

          $scope.results = results.data.data.data.slice(2, 7);
          $scope.toggleGroup($scope.results[0]);
      });

      $scope.toggleGroup = function (group) {
          if ($scope.isGroupShown(group)) {
              $scope.shownGroup = null;
          } else {
              $scope.shownGroup = group;
          }
      };

      $scope.isGroupShown = function (group) {
          return $scope.shownGroup === group;
      };

      function orderResultByRent() {

      };

      $scope.logOut = function () {
          $state.go('splash');
      };
  })


  /*
  Controller for our tab bar
  */
  .controller('TabsCtrl', function ($scope, $window, User) {
    $scope.logout = function () {
      User.destroySession();
      $window.location.href = 'index.html';
    }
  });
